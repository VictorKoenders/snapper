
<a name="0.0.1"></a>
## [Unreleased](https://gitlab.com/rust-community-matrix/snapper/compare/v0.0.0...HEAD)

> 2021-11-01

### Code Refactoring

* **box:** Move modules into crypto module

### Features

* **box:** Create file module
* **box:** Add derive_with_context method
* **box:** Add EncryptedDerivedKey struct
* **box:** Add cipher and plain text abstractions
* **box:** Add DerivedKey type
* **box:** Implement RootKey and associated types
* **box:** Add dependencies and sketch key module
* **box:** Create box module


<a name="v0.0.0"></a>
## v0.0.0

> 2021-10-29
