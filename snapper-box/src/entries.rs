//! Data types for the control structure of a [`CryptoBox`](crate::CryptoBox)

use std::sync::Arc;

use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::crypto::DerivedKey;

/// Can contain any control structure in a [`CryptoBox`](crate::CryptoBox)
#[derive(Clone, Serialize, Deserialize)]
#[non_exhaustive]
pub enum Control {
    /// Stores the default settings for the [`CryptoBox`](crate::CryptoBox)
    ///
    /// This lives at the empty string (`""`) path in the root namespace
    Settings(Settings),
    /// List of namespaces in the [`CryptoBox`](crate::CryptoBox)
    ///
    /// This lives at the `"namespaces"` path in the root namespace
    Namespaces(Namespaces),
}

/// Initial configuration settings for a [`CryptoBox`](crate::CryptoBox)
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Settings {
    /// Compression level for this box
    pub compression: Option<i32>,
    /// Default maximum write cache entries for this box
    pub max_cache_entries: Option<usize>,
}

/// The namespaces control structure
#[derive(Clone, Serialize, Deserialize)]
pub struct Namespaces {
    /// The list of namespaces
    pub namespaces: Vec<Namespace>,
}

/// Information about a Namespace
#[derive(Hash, Clone, Serialize, Deserialize)]
pub struct Namespace {
    /// The name of this namespace
    pub name: String,
    /// The derived key for this namespace
    pub key: Arc<DerivedKey>,
    /// The [`Uuid`] of this namespace
    pub uuid: Uuid,
}
