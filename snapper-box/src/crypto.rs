//! Types and traits serving as the cryptographic primitives of `snapper-box`'s cryptosystem. <span
//! style="color:red">**HAZMAT**</span>
//!
//! This module provides both originating keys ([`RootKey`]) and derived keys ([`DerivedKey`]) as well
//! as nonce types and other hazardous material.
//!
//! Originating keys ([`RootKey`]) are randomly generated, and have methods for their usage in
//! generating derived keys, as well methods for encrypting the key with a password for safe at-rest
//! storage.
//!
//! Derived keys ([`DerivedKey`]) are generated via a KDF over the originating key's entropy pool and a
//! context string incorporating both a random nonce and a namespace.
//!
//! This module also provides wrapper types for shuffling data back and forth between encrypted and
//! unencrypted representations, as well as the ability to, optionally, transparently compress plaintext
//! before encryption.
//!
//! # <span style="color:red">**DANGER**</span>
//!
//! This module deals in low level cryptographic details, and it can be incredibly dangerous to interact
//! with directly. It is advisable to not deal with this module directly, and instead use a higher level
//! API.

mod key;
mod types;

#[doc(inline)]
pub use key::{DerivedKey, EncryptedDerivedKey, EncryptedRootKey, Key, Nonce, RootKey};
#[doc(inline)]
pub use types::{CipherText, ClearText};
