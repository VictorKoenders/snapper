![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/rust-community-matrix/snapper?branch=trunk&style=flat-square)
[![Crates.io](https://img.shields.io/crates/v/snapper?label=snapper&logo=rust&style=flat-square)](https://crates.io/crates/snapper)
[![Crates.io](https://img.shields.io/crates/v/snapper-client?label=snapper-client&logo=rust&style=flat-square)](https://crates.io/crates/snapper-client)
[![Matrix](https://shields.io/badge/Matrix-%23snapper%3Acommunity.rs-green?logo=matrix&style=flat-square)](https://matrix.to/#/#snapper:community.rs)
![Crates.io](https://img.shields.io/crates/l/snapper?style=flat-square)

# Snapper

<div align="center">
  <img src="https://gitlab.com/rust-community-matrix/snapper/-/raw/trunk/static/snapper.png" alt="Actually  a box turtle" width="350" height="350" />
</div>

## About

Snapper is a family of crates for implementing [matrix](https://matrix.org/) utility and
administration bots in Rust.

## Project structure

Snapper is divided into the following crates:
  * `snapper-box` - Encrypted persistent store used by `snapper`
  * `snapper-client` - Low level, bot oriented matrix client
  * `snapper` - High level bot api
  * `snapper-bot` - Reference bot implemented with `snapper`

## Built With
Under the hood, `snapper` uses:
  * executor - [`async_std`](https://github.com/async-rs/async-std)
  * http client - [`surf`](https://github.com/http-rs/surf) in curl client mode

## Getting started

TODO

### Prerequisites

This project uses [nix](https://nixos.org/) for development. While it does use a [nix
flake](https://nixos.wiki/wiki/Flakes), the repository makes use of compatibility shims to allow
development and building without needing to be on `unstableNix`.

Once you [have nix installed](https://nixos.org/download.html), simply run, if you have flakes and
nix-command enabled:

``` shell
nix develop
```

or if you do not:

``` shell
nix-shell
```

This project also has a `.evnrc` file, for use with direnv.

## Usage

TODO

## Contributing

Take a look at [`CONTRIBUTING.md`](CONTRIBUTING.md)


## Documentation

Please take a look at the [`CHANGELOG.md`](CHANGELOG.md) and the rustdoc.

### Rustdoc

The up to date, current trunk rustdoc for each crate can be found at the following locations:
  * [`snapper-box`](https://rust-community-matrix.gitlab.io/snapper/snapper_box/index.html)
  * [`snapper-client`](https://rust-community-matrix.gitlab.io/snapper/snapper_client/index.html)
  * [`snapper`](https://rust-community-matrix.gitlab.io/snapper/snapper/index.html)
  * [`snapper-bot`](https://rust-community-matrix.gitlab.io/snapper/snapper_bot/index.html)
