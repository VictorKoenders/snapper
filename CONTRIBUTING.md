# Prerequisites

This project uses [nix](https://nixos.org/) for development. While it does use a [nix
flake](https://nixos.wiki/wiki/Flakes), the repository makes use of compatibility shims to allow
development and building without needing to be on `unstableNix`.

Once you [have nix installed](https://nixos.org/download.html), simply run, if you have flakes and
nix-command enabled:

``` shell
nix develop
```

or

``` shell
nix-shell
```

if you do not.

This project also has a `.evnrc` file, for use with direnv.
