use color_eyre::eyre::Result;
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

#[async_std::main]
async fn main() -> Result<()> {
    // Setup error handler
    color_eyre::install()?;
    // Setup tracing
    tracing_subscriber::registry()
        .with(EnvFilter::from_default_env())
        .with(fmt::layer().pretty())
        .init();
    Ok(())
}
