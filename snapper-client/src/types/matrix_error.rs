use serde::{Deserialize, Serialize};

/// Error kind's that can occur in matrix
///
/// TODO(#4): Implement all the error types
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub enum ErrorKind {
    /// M_FORBIDDEN
    #[serde(rename(serialize = "M_FORBIDDEN", deserialize = "M_FORBIDDEN"))]
    Forbidden,
    /// M_UNKNOWN_TOKEN
    #[serde(rename(serialize = "M_UNKNOWN_TOKEN", deserialize = "M_UNKNOWN_TOKEN"))]
    UnkownToken,
}

/// A matrix error
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct MatrixError {
    /// The type of the error
    pub errcode: ErrorKind,
    /// User friendly error message
    pub message: String,
}
