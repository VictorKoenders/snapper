#![warn(
    clippy::all,
    clippy::pedantic,
    rust_2018_idioms,
    missing_docs,
    clippy::missing_docs_in_private_items
)]
#![allow(
    clippy::option_if_let_else,
    clippy::module_name_repetitions,
    clippy::shadow_unrelated,
    clippy::must_use_candidate,
    clippy::implicit_hasher
)]
// Pull doc string from readme
#![doc = include_str!("../README.md")]
#![doc(
    html_logo_url = "https://gitlab.com/rust-community-matrix/snapper/-/raw/trunk/static/snapper.png"
)]

pub mod state;
/// Representations of types used by the matrix api
pub mod types;

use std::{path::Path, sync::Arc};

use async_std::sync::RwLock;
pub use async_trait;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use snafu::{OptionExt, ResultExt};
pub use snapper_box;
use snapper_box::{async_wrapper::AsyncCryptoBox, CryptoBox};
pub use surf;
use surf::Url;
use tracing::{info, instrument};

use crate::{
    state::LoginState,
    types::{
        identifiers::{AuthenticationToken, UserId},
        traits::request::{Header, Request, RequestError, Response},
        ClientError, Cryptography, HomeServerUrl, MissingConfig,
    },
};

/// State of a currently running client
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct ClientState {
    /// Current login status of the client
    pub login_state: LoginState,
    /// Userid of the client
    pub user_id: UserId,
    /// Homeserver api endpoint
    pub homeserver_url: Url,
}

/// A currently running client
#[derive(Clone)]
pub struct Client {
    /// Current state of the client
    state: Arc<RwLock<ClientState>>,
    /// Currently active surf client
    surf_client: surf::Client,
    /// State store for the client
    store: Arc<RwLock<AsyncCryptoBox>>,
}

impl Client {
    /// Create a new client
    ///
    /// This will start up the background http client, as well as opening a [`CryptoBox`] store at the
    /// provided path. The [`CryptoBox`] defaults to a zstd compression level of 3, and the default write
    /// cache size.
    ///
    /// # Arguments
    ///
    ///   * `path` - The location to place the [`CryptoBox`] store
    ///
    ///      This must be a directory that the user has write access to, and the directory must not exist
    ///      yet.
    ///   * `store_password` - The password to set for this store
    ///   * `compression` - The default compression level to set for this store. Defaults to no compression.
    ///
    /// # Errors
    ///
    ///   * [`ClientError::Cryptography`] if any errors occur within the [`CryptoBox`] store
    #[instrument(skip(path, store_password), err)]
    pub async fn init(
        path: impl AsRef<Path>,
        user_id: UserId,
        homeserver: impl AsRef<str> + std::fmt::Debug,
        store_password: impl AsRef<str>,
        compression: Option<i32>,
    ) -> Result<Client, ClientError> {
        let surf_client = surf::client();
        // Decode the homeserver url
        let homeserver_str = homeserver.as_ref();
        let homeserver_url = Url::parse(homeserver_str).context(HomeServerUrl)?;
        // Create the store and create the namespaces
        let path = path.as_ref();
        info!(?path, "Opening Client");
        let mut store = AsyncCryptoBox::new(
            CryptoBox::init(path, compression, None, store_password.as_ref())
                .context(Cryptography)?,
        );
        // Create the client namespace
        store
            .create_namespace("client".to_string())
            .await
            .context(Cryptography)?;
        // Write a login state
        store
            .insert(&"login_state", &LoginState::LoggedOut, "client")
            .await
            .context(Cryptography)?;
        // Write the username
        store
            .insert(&"user_id", &user_id, "client")
            .await
            .context(Cryptography)?;
        // Write the homeserver
        store
            .insert(&"homeserver_url", &homeserver_url, "client")
            .await
            .context(Cryptography)?;
        store.flush().await.context(Cryptography)?;
        let state = ClientState {
            login_state: LoginState::LoggedOut,
            user_id,
            homeserver_url,
        };
        Ok(Client {
            state: Arc::new(RwLock::new(state)),
            surf_client,
            store: Arc::new(RwLock::new(store)),
        })
    }

    /// Reopens an existing client
    ///
    /// This will start up the background http client, as well as opening the [`CryptoBox`] store at the
    /// provided path.
    ///
    /// This will automatically load persisted client state and configuration from the store.
    ///
    /// # Arguments
    ///
    ///   * `path` - Path the existing store is located in
    ///   * `store_password` - The password set for this store
    ///
    /// # Errors
    ///
    ///   * [`ClientError::Cryptography`] if any errors occur within the [`CryptoBox`] store
    #[instrument(skip(path, store_password), err)]
    pub async fn open(
        path: impl AsRef<Path>,
        store_password: impl AsRef<str>,
    ) -> Result<Client, ClientError> {
        let surf_client = surf::client();
        // Create the store and create the namespaces
        let path = path.as_ref();
        info!(?path, "Opening Client");
        let mut store = AsyncCryptoBox::new(
            CryptoBox::open(path, store_password.as_ref()).context(Cryptography)?,
        );
        // Read the login state
        let login_state: LoginState = store
            .get(&"login_state", "client")
            .await
            .context(Cryptography)?
            .context(MissingConfig {
                name: "login_state",
            })?;
        // Read the user id
        let user_id: UserId = store
            .get(&"user_id", "client")
            .await
            .context(Cryptography)?
            .context(MissingConfig { name: "user_id" })?;
        // Read the homeserver url
        let homeserver_url: Url = store
            .get(&"homeserver_url", "client")
            .await
            .context(Cryptography)?
            .context(MissingConfig {
                name: "homeserver_url",
            })?;
        let state = ClientState {
            login_state,
            user_id,
            homeserver_url,
        };
        Ok(Client {
            state: Arc::new(RwLock::new(state)),
            surf_client,
            store: Arc::new(RwLock::new(store)),
        })
    }

    /// Gets a copy of the client's current authentication token, if the bot is logged in
    pub async fn get_auth_token(&self) -> Option<AuthenticationToken> {
        let state = self.state.read().await;
        state.login_state.get_auth_token().cloned()
    }

    /// Gets a copy of this client's [`UserId`]
    pub async fn get_user_id(&self) -> UserId {
        let state = self.state.read().await;
        state.user_id.clone()
    }

    /// Gets a copy of this client's homeserver [`Url`]
    pub async fn get_homeserver_url(&self) -> Url {
        let state = self.state.read().await;
        state.homeserver_url.clone()
    }

    /// Get an item from the specified namespace in the [`CryptoBox`]
    ///
    /// This allow you to read values in any namespace in the [`CryptoBox`], even reserved ones.
    ///
    /// This method will automatically create the namespace if it does not exist.
    ///
    /// # Errors
    ///
    /// Will return [`ClientError::Cryptography`] if any underlying errors occur.
    pub async fn get<K, V>(
        &self,
        key: &K,
        namespace: impl AsRef<str>,
    ) -> Result<Option<V>, ClientError>
    where
        K: Serialize,
        V: DeserializeOwned,
    {
        let mut crypto_box = self.store.write().await;
        crypto_box
            .get(key, namespace.as_ref())
            .await
            .context(Cryptography)
    }

    /// Insert an item into the specified namespace in the [`CryptoBox`].
    ///
    /// This will only allow insertion into non-reserved namespaces, attempts to insert into a reserved
    /// namespace will fail with an error. At present, the client reserves all the namespaces starting with
    /// `"client"`, case and whitespace insensitively .
    ///
    /// # Errors
    ///
    ///   * [`ClientError::Cryptography`] - If an underlying error occurs with the [`CryptoBox`]
    ///   * [`ClientError::ReservedNamespace`] - If the specified namespace is reserved
    pub async fn insert<K, V>(
        &self,
        key: &K,
        value: &V,
        namespace: impl AsRef<str>,
    ) -> Result<(), ClientError>
    where
        K: Serialize,
        V: Serialize,
    {
        // Make sure the namespace isn't reserved
        let namespace = namespace.as_ref();
        is_reserved_namespace(namespace)?;
        // Call through to internal method
        self.insert_reserved(key, value, namespace).await
    }

    /// Internal method for inserting into reserved namespaces
    ///
    /// # Errors
    ///
    ///   * [`ClientError::Cryptograhpy`] - If an underlying error occurs with the [`CryptoBox`]
    pub(crate) async fn insert_reserved<K, V>(
        &self,
        key: &K,
        value: &V,
        namespace: impl AsRef<str>,
    ) -> Result<(), ClientError>
    where
        K: Serialize,
        V: Serialize,
    {
        let mut crypto_box = self.store.write().await;
        let namespace = namespace.as_ref();
        // Create the namespace if it doesn't exits
        if crypto_box
            .namespace_exists(namespace)
            .await
            .context(Cryptography)?
        {
            // Namespace exists, directly insert
            crypto_box
                .insert(key, value, namespace)
                .await
                .context(Cryptography)
        } else {
            // Create namespace
            crypto_box
                .create_namespace(namespace.to_string())
                .await
                .context(Cryptography)?;
            // Direct insert
            crypto_box
                .insert(key, value, namespace)
                .await
                .context(Cryptography)
        }
    }

    /// Replaces the internal surf client with the provided one
    ///
    /// This is useful for testing and for installing your own middleware.
    pub fn with_surf_client(self, surf: surf::Client) -> Self {
        Self {
            surf_client: surf,
            ..self
        }
    }

    /// Flushes the backing storage
    ///
    /// # Errors
    ///
    /// Will return [`ClientError::Cryptography`] if any underlying errors occur
    pub async fn flush(&self) -> Result<(), ClientError> {
        let mut crypto_box = self.store.write().await;
        crypto_box.flush().await.context(Cryptography)
    }

    /// Returns the correct header value for the current state of the client

    /// Performs the provided request
    ///
    /// Sets up the header to include the authentication token, if there is one, and any other relevant
    /// state, and issues it.
    ///
    /// Before returning the response, the `Client` will evaluate the response's state transition and apply
    /// it, keeping the state up to date with any new information that is returned.
    ///
    /// # Errors
    ///
    /// This method will error if any underlying errors occur.
    #[instrument(skip(self))]
    pub async fn request<T>(&self, request: T) -> Result<T::Response, RequestError<T::Error>>
    where
        T: Request + std::fmt::Debug,
    {
        let homeserver_url = self.get_homeserver_url().await;
        let authorizaton = self.get_auth_token().await;
        let headers = Header { authorizaton };
        let result = request
            .perform_request(&homeserver_url, &headers, &self.surf_client)
            .await?;

        result.state_transition(self.clone()).await?;
        Ok(result)
    }
}

/// Method to determine if a namespace is reserved or not
///
/// Currently, the client reserves all namespaces whose name's start with `client`, without regard to
/// case sensitivity or leading whitespace
///
/// # Errors
///
/// This will return an error if the namespace is invalid
pub fn is_reserved_namespace(namespace: &str) -> Result<(), ClientError> {
    if namespace.trim_start().to_lowercase().starts_with("client") {
        Err(ClientError::ReservedNamespace {
            name: namespace.to_string(),
        })
    } else {
        Ok(())
    }
}

/// Unit tests for the `Client` struct
#[cfg(test)]
mod root_tests {
    use super::*;
    use tempfile::tempdir;
    /// Store password used in testing
    const STORE_PASSWORD: &str = "password";

    /// Initialization, opening, and logging in
    mod init {
        use super::*;
        /// Test initializing a client, closing it, and reopening it
        #[async_std::test]
        async fn init_close_open() -> Result<(), ClientError> {
            let tempdir = tempdir().expect("Unable to open tempdir");
            let path = tempdir.path().join("client");
            // Initialize the client
            let client = Client::init(
                &path,
                UserId::try_from("@snapper:community.rs".to_string()).unwrap(),
                "https://matrix.community.rs",
                STORE_PASSWORD,
                None,
            )
            .await?;
            // Drop it to close
            client.flush().await?;
            std::mem::drop(client);
            // Reopen it
            let _client = Client::open(&path, STORE_PASSWORD).await?;
            Ok(())
        }
    }

    /// Passthrough methods to underlying `CryptoBox`
    mod crypto_box {
        use super::*;

        /// Make sure that the reserved namespaces are actually reserved
        #[async_std::test]
        async fn reserved_namespaces() -> Result<(), ClientError> {
            // Get a client
            let tempdir = tempdir().expect("Unable to open tempdir");
            let path = tempdir.path().join("client");
            // Initialize the client
            let client = Client::init(
                &path,
                UserId::try_from("@snapper:community.rs".to_string()).unwrap(),
                "https://matrix.community.rs",
                STORE_PASSWORD,
                None,
            )
            .await?;
            // Make sure writes to a non-reserved namespace work
            client
                .insert(&"Testing key", &"Testing Value", "testing-namespace")
                .await?;
            // Write to the `client` namespace should fail
            let ret = client
                .insert(&"Testing key", &"Testing Value", "client")
                .await;
            assert!(ret.is_err());
            // Write to `client:an-inner-namespace` should fail
            let ret = client
                .insert(
                    &"Testing key",
                    &"Testing Value",
                    "client:an-inner-namespace",
                )
                .await;
            assert!(ret.is_err());
            // We should be able to write to the client namespace with insert_reserved
            client.insert_reserved(&"key", &"value", "client").await?;
            // Then we should be able to get that value back out
            let ret: Option<String> = client.get(&"key", "client").await?;
            assert_eq!(ret, Some("value".to_string()));
            Ok(())
        }

        /// Basic insertion/retrival tests
        #[async_std::test]
        async fn basic_insert_get() -> Result<(), ClientError> {
            // Get a client
            let tempdir = tempdir().expect("Unable to open tempdir");
            let path = tempdir.path().join("client");
            // Initialize the client
            let client = Client::init(
                &path,
                UserId::try_from("@snapper:community.rs".to_string()).unwrap(),
                "https://matrix.community.rs",
                "testing",
                None,
            )
            .await?;
            // Insert some values
            client.insert(&"key_1", &"value_1", "test").await?;
            client.insert(&"key_2", &"value_2", "test").await?;
            client.insert(&"key_3", &"value_3", "test").await?;
            client.insert(&"key_1", &"value_4", "test_two").await?;
            // Get them back out and check for correctness
            assert_eq!(
                Some("value_1".to_string()),
                client.get(&"key_1", "test").await?.unwrap()
            );
            assert_eq!(
                Some("value_2".to_string()),
                client.get(&"key_2", "test").await?.unwrap()
            );
            assert_eq!(
                Some("value_3".to_string()),
                client.get(&"key_3", "test").await?.unwrap()
            );
            assert_eq!(
                Some("value_4".to_string()),
                client.get(&"key_1", "test_two").await?.unwrap()
            );
            // Overwrite a value, and try again
            client.insert(&"key_2", &"value_5", &"test").await?;
            // Get them back out and check for correctness
            assert_eq!(
                Some("value_1".to_string()),
                client.get(&"key_1", "test").await?.unwrap()
            );
            assert_eq!(
                Some("value_5".to_string()),
                client.get(&"key_2", "test").await?.unwrap()
            );
            assert_eq!(
                Some("value_3".to_string()),
                client.get(&"key_3", "test").await?.unwrap()
            );
            assert_eq!(
                Some("value_4".to_string()),
                client.get(&"key_1", "test_two").await?.unwrap()
            );
            Ok(())
        }
    }
}
