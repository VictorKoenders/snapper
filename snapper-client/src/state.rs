//! Representations of internal client state

use serde::{Deserialize, Serialize};

use crate::types::identifiers::AuthenticationToken;

/// Current login state of the client
#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum LoginState {
    /// Client is not currently logged in
    LoggedOut,
    /// Client is logged in
    LoggedIn {
        /// Token the client is logged in with
        auth_token: AuthenticationToken,
    },
}

impl LoginState {
    /// Gets a reference to the authentication token, if one exists
    pub fn get_auth_token(&self) -> Option<&AuthenticationToken> {
        match self {
            LoginState::LoggedOut => None,
            LoginState::LoggedIn { auth_token } => Some(auth_token),
        }
    }
    /// Makes a new, logged out, login state
    pub fn logged_out() -> Self {
        Self::LoggedOut
    }
    /// Makes a new, logged in, login state
    pub fn logged_in(auth_token: AuthenticationToken) -> Self {
        Self::LoggedIn { auth_token }
    }
}

impl Default for LoginState {
    fn default() -> Self {
        LoginState::LoggedOut
    }
}
