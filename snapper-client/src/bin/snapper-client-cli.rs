use std::path::PathBuf;

use color_eyre::eyre::{Result, WrapErr};
use structopt::{clap::AppSettings, StructOpt};
use tracing::{debug, instrument};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

use snapper_client::{
    types::identifiers::{Identifer, Password, UserId},
    Client,
};

const AUTHORS: &str = env!("CARGO_PKG_AUTHORS");
const AFTER_HELP: &str =
    "This is dangerous, experimental software. It is fully entitled to eat your laudry. Use with caution";

#[derive(Debug, StructOpt)]
#[structopt(
    name = "Snapper Client CLI",
    about = "Commandline interface for the Snapper Client",
    after_help = AFTER_HELP,
    author = AUTHORS,
    global_settings = &[AppSettings::ColoredHelp],
)]
struct Opt {
    /// Location of the snapper client data directory
    #[structopt(name = "STORE_PATH", env = "SNAPPER_CLIENT_STORE")]
    store_path: PathBuf,
    /// Password to the repository
    #[structopt(long, short, env = "SNAPPER_STORE_PASSWORD", hide_env_values = true)]
    password: String,
    #[structopt(subcommand)]
    cmd: Sub,
}

/// Enum for subcommands
#[derive(Debug, StructOpt)]
enum Sub {
    /// Initialize the client state and configuration
    #[structopt(
        after_help = AFTER_HELP,
        author = AUTHORS,
    )]
    Init {
        /// ZStd compression level to use for the store.
        ///
        /// Defaults to no compression
        #[structopt(long, short)]
        compression: Option<i32>,
        /// The homeserver url to use
        #[structopt(long, short, env = "SNAPPER_HOMESERVER")]
        homeserver_url: String,
        /// The UserId to use
        #[structopt(long, short, env = "SNAPPER_USER_ID")]
        user_id: String,
    },
    /// Login to the homeserver, and store an access token
    #[structopt(
        after_help = AFTER_HELP,
        author = AUTHORS,
    )]
    Login {
        /// The password to login with
        #[structopt(long, short, env = "SNAPPER_PASSWORD")]
        password: Option<String>,
    },
}

#[instrument]
#[async_std::main]
async fn main() -> Result<()> {
    // Install error handler
    color_eyre::install()?;
    // Setup tracing
    tracing_subscriber::registry()
        .with(EnvFilter::from_default_env())
        .with(fmt::layer().pretty())
        .init();
    // Parse commandline arguments
    let opt = Opt::from_args();
    debug!(?opt);
    let store_path = opt.store_path.join("store");
    let store_password = &opt.password;
    // Branch
    match opt.cmd {
        Sub::Init {
            compression,
            homeserver_url,
            user_id,
        } => {
            let client = Client::init(
                &store_path,
                UserId::try_from(user_id).wrap_err("Invalid UserId")?,
                homeserver_url,
                store_password,
                compression,
            )
            .await
            .wrap_err("Failed to init client")?;
            client.flush().await.wrap_err("Failed to write client")?;
            println!("Initialized client");
        }
        Sub::Login { password } => {
            use snapper_client::types::login::Login;
            // Open the client up first
            let client = Client::open(&store_path, store_password)
                .await
                .wrap_err("Failed to open client")?;
            // Get userid and password
            let user = client.get_user_id().await;
            let password = if let Some(password) = password {
                password
            } else {
                let prompt = format!("Enter password for {}:", &*user);
                rpassword::read_password_from_tty(Some(&prompt))
                    .wrap_err("Failed to read password")?
            };
            // Prepare a login request
            let identifier = Identifer::UserId { user };
            let password = Password::from(password);
            let login = Login::Password {
                identifier,
                password,
                device_id: None,
            };
            // Have the client login
            client
                .request(login)
                .await
                .wrap_err("Failed login request")?;

            // Flush the client
            client.flush().await.wrap_err("Failed to write client")?;
            // Print the access token
            println!(
                "Login complete.\nAccess Token: {:?}",
                client.get_auth_token().await
            );
        }
    }
    Ok(())
}
