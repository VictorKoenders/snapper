use snafu::Snafu;

/// Identifier types
pub mod identifiers;
/// Login types
/// ([`m.login`](https://matrix.org/docs/spec/client_server/latest#user-interactive-authentication-api))
pub mod login;
/// Matrix protocol errors
pub mod matrix_error;
/// General traits module
pub mod traits;

/// Error type for grammar mismatch errors
#[derive(Debug, Snafu, PartialEq, Eq)]
#[non_exhaustive]
pub enum GrammarMismatch {
    /// Identifier does not contain an `@`
    #[snafu(display("Given identifier does not start with an @: {}", id))]
    NoAt {
        /// The identifier that failed to parse
        id: String,
    },
    /// Identifier does not contain a `:`
    #[snafu(display("Given identifier does not contain an `:` : {}", id))]
    NoColon {
        /// The identifier that failed parse
        id: String,
    },
    /// Identifier contains an invalid character
    #[snafu(display("Identifier contains an invalid character (`{:?}`): {}", ch, id))]
    InvaildChar {
        /// The identifier that failed to parse
        id: String,
        /// The invalid character
        ch: char,
    },
    /// Missing a component
    #[snafu(display("Identifier lacks component: {}", id))]
    MissingComponent {
        /// The identifier that failed to parse
        id: String,
    },
    /// Too long
    #[snafu(display("Identifier was too long (max: {}, actual: {})", max, actual))]
    TooLong {
        /// maximum length
        max: usize,
        /// actual length
        actual: usize,
    },
}

/// Error type for `Client` operations
#[derive(Debug, Snafu)]
#[non_exhaustive]
#[snafu(visibility = "pub(crate)")]
pub enum ClientError {
    /// A cryptographic store error occurred
    Cryptography {
        /// The underlying error
        source: snapper_box::error::CryptoBoxError,
    },
    /// The store is missing a configuration value
    #[snafu(display("Store missing required configuration value: {}", name))]
    MissingConfig {
        /// The name of the missing value
        name: &'static str,
    },
    /// Attempted to modify a namespace that is reserved by the bot
    #[snafu(display(
        "Attempted to modify a namespace that is reserved by the bot: {}",
        name
    ))]
    ReservedNamespace {
        /// The name of the reserved namespace
        name: String,
    },
    /// An error occured while handling a request
    #[snafu(display("An error occured while handling the request: {}", error))]
    RequestError {
        /// the underlying error
        error: Box<dyn std::error::Error + Send + Sync + 'static>,
    },
    /// Invalid Homeserver URL
    HomeServerUrl {
        /// The underlying error
        source: surf::http::url::ParseError,
    },
}
