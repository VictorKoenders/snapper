![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/rust-community-matrix/snapper?branch=trunk&style=flat-square)
[![Crates.io](https://img.shields.io/crates/v/snapper-client?style=flat-square)](https://crates.io/crates/snapper-client)
[![Matrix](https://shields.io/badge/Matrix-%23snapper%3Acommunity.rs-green?logo=matrix&style=flat-square)](https://matrix.to/#/#snapper:community.rs)
![Crates.io](https://img.shields.io/crates/l/snapper-client?style=flat-square)

# Snapper Client

<div align="center">
  <img src="https://gitlab.com/rust-community-matrix/snapper/-/raw/trunk/static/snapper.png" alt="Actually  a box turtle" width="350" height="350" />
</div>

## About

`snapper-client` is a bot-biased, A bot-biased,
[`async_std`](https://docs.rs/async-std/1.10.0/async_std/) and
[`surf`](https://docs.rs/surf/2.3.1/surf/) based matrix client.

This crate makes use of `CryptoBox` from `snapper-box` as a backing store.

## Usage

TODO

## Contributing

Take a look at
[`CONTRIBUTING.md`](https://gitlab.com/rust-community-matrix/snapper/-/blob/trunk/CONTRIBUTING.md)


## Documentation

Please take a look at the
[`CHANEGLOG.md`](https://gitlab.com/rust-community-matrix/snapper/-/blob/trunk/snapper-client/CHANGELOG.md)
and the rustdoc.
