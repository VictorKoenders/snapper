{
  description = "The turtle-reborn matrix utility bot";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    crate2nix = {
      url = "github:kolloch/crate2nix";
      flake = false;
    };
    # Pinned version used for stable rust
    # Currently pinned to 1.56.1
    fenix = {
      url = "github:nix-community/fenix?rev=84a5090408d5fc455729cad582496c2af9cace57";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Free floating, used for nightly rust
    fenix-nightly = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-compat, utils, crate2nix, fenix, fenix-nightly }:
    utils.lib.eachDefaultSystem (system:
      let
        fenixPackage = fenix.packages.${system}.stable.withComponents [ "cargo" "clippy" "rust-src" "rustc" "rustfmt" ];
        fenixNightlyPackage = fenix-nightly.packages.${system}.complete.withComponents [ "cargo" "clippy" "rust-src" "rustc" "rustfmt" ];
        rustOverlay = final: prev:
          {
            inherit fenixPackage;
            rustc = fenixPackage;
            cargo = fenixPackage;
            rust-src = fenixPackage;
          };

        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            rustOverlay
          ];
        };

        crateName = "snapper";

        inherit (import "${crate2nix}/tools.nix" { inherit pkgs; })
          generatedCargoNix;

        project = import
          (generatedCargoNix {
            name = crateName;
            src = ./.;
          })
          {
            inherit pkgs;
            defaultCrateOverrides = pkgs.defaultCrateOverrides // {
              # Crate dependency overrides go here
              # olm-sys = attrs: {
              #   nativeBuildInputs = [ pkgs.cmake ];
              #   buildInputs = [ pkgs.olm ];
              #   OLM_LINK_VARIANT = "dylib";
              # };
            };
          };

      in
      {
        packages.${crateName} = pkgs.symlinkJoin {
          name = "snapper-collection";
          paths =
            let members = builtins.attrValues project.workspaceMembers;
            in builtins.map (m: m.build.override { features = [ "default" "all" ]; }) members;
        };

        packages.tests.${crateName} = pkgs.symlinkJoin {
          name = "snapper-collection-tests";
          paths =
            let members = builtins.attrValues project.workspaceMembers;
            in builtins.map (m: m.build.override { runTests = true; features = [ "default" "all" ]; }) members;
        };

        defaultPackage = self.packages.${system}.${crateName};

        devShell = pkgs.mkShell {
          inputsFrom = builtins.attrValues self.packages.${system};
          buildInputs =
            with pkgs; [ cargo-audit nixpkgs-fmt git-chglog openssl pkgconfig fenix-nightly.packages.${system}.rust-analyzer cmake fenixPackage cargo-release pre-commit gitFull git-lfs cargo-udeps cbor-diag cargo-criterion ];
        };

        packages.nightlyRustShell = pkgs.mkShell {
          buildInputs =
            with pkgs; [ cargo-audit nixpkgs-fmt git-chglog openssl pkgconfig fenix-nightly.packages.${system}.rust-analyzer cmake fenixNightlyPackage cargo-release pre-commit gitFull git-lfs cargo-tarpaulin cargo-udeps cbor-diag cargo-criterion ];
        };
      });
}
